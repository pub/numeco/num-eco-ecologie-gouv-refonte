import {newAnalysisCommun, convertJsonToList} from './analysis.js';
import fs from 'fs';

if (process.env.DEBUG) {
  newAnalysisCommun(3);
} else {
  fs.access("../target/actu.json", function (error) {
    if (error) {
      console.log("File does not exist.") // Directory target
      throw error;
    }
  })
  const pathActu = '../target/actu.json'
  fs.access("../target/rdv.json", function (error) {
    if (error) {
      console.log("File does not exist.");
      throw error;
    }
  })
  const pathRdv = '../target/rdv.json';
  fs.access("../target/dossiers_presse.json", function (error) {
    if (error) {
      console.log("File does not exist.")
      throw error;
    }
  })
  const pathDossiersPresse = '../target/dossiers_presse.json';
  newAnalysisCommun(await convertJsonToList(pathActu), await convertJsonToList(pathRdv), await convertJsonToList(pathDossiersPresse));
}


