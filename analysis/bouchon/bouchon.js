import express from 'express';
import { readFile } from 'fs';
import { join } from 'path';

const app = express();
const port = 3000;
const timeoutUrl = "/classement-du-site-des-mont-sery";
const timeoutUrlSometimes = "/appel-projets-repeuplement-languille";
let timeoutSometimesCount = 0;

app.use((req, res, next) => {
  setTimeout(() => {
    next();
  }, 1000);
});

// Middleware pour servir le contenu HTML du fichier local
app.get('/actualites', (req, res) => {
  const filePath = './bouchon/actu_mte.html';

  readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.send(data);
    }
  });
});

app.get(timeoutUrl, (req, res) => {
  setTimeout(() => {
    res.set('Content-Type', 'application/json')
    res.status(200).send('Hello Timeout')
  }, 100000)
});

app.get(timeoutUrlSometimes, (req, res) => {
  timeoutSometimesCount += 1;
  if (timeoutSometimesCount % 2 == 0) {
    res.send(`Hello ${timeoutSometimesCount}`);
  } else {
    setTimeout(() => {
      res.set('Content-Type', 'application/json')
      res.status(200).send('Hello Timeout')
    }, 100000);
  }
});

app.use((req, res) => {
  res.send('Hello World');
});

// Démarrer le serveur
app.listen(port, () => {
  console.log(`Le serveur écoute sur le port ${port}`);
  console.log(`Toutes les requêtes renvoient une réponse au bout de 1s sauf l'actu n°3 qui renvoie au bout de 100s.`)
});
