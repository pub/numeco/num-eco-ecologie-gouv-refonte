<!-- Adapted from https://github.com/othneildrew/Best-README-Template/ -->
<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<div align="center">

[![puppeteer][puppeteer]][Puppeteer-url]
[![mkdocs][mkdocs]][mkdocs-url]

<h3 align="center">Analyse d'impact numérique du site ecologie.gouv.fr</h3>

  <p align="center">
    Un suivi des poids des pages et des médias dans ecologie.gouv.fr
    <br />
</p>
</div>

> Le suivi s'effectue sur la [page gitlab de ce projet](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/numeco/numeco-ecologie-gouv/)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[license-shield]: https://img.shields.io/github/license/geoffreyarthaud/gitlab-project-doctor.svg?style=for-the-badge

[license-url]: https://github.com/geoffreyarthaud/gitlab-project-doctor/blob/master/LICENSE.txt

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555

[linkedin-url]: https://linkedin.com/in/geoffreyarthaud

[product-screenshot]: docs_assets/gpd_screenshot.png

[mkdocs]: https://img.shields.io/badge/mkdocs-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54

[mkdocs-url]: https://www.mkdocs.org/

[puppeteer]: https://img.shields.io/badge/puppeteer-000000?style=for-the-badge&logo=javascript&logoColor=01d9a3

[puppeteer-url]: https://pptr.dev/

[MTE]: https://img.shields.io/badge/forge%20MTE-0000?color=00008f&style=for-the-badge&logo=gitlab

[MTE-url]: https://gitlab-forge.din.developpement-durable.gouv.fr/snum/dam/pot/naos-numeco

